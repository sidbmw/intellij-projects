import java.io.*;

public class IO2 {
    public static PrintWriter fileOut;
    public static BufferedReader fileIn;

    public void createFile(String fileName) {
        try {
            fileOut = new PrintWriter(fileName, "UTF-8");
        } catch(IOException ex) {
            System.out.println("***Cannot create " + fileName + "***");
        }
    }

    public void openFile(String fileName) {
        try {
            fileIn = new BufferedReader(new FileReader(fileName));
        } catch (FileNotFoundException e) {
            System.out.println("***Cannot open " + fileName + "***");
        }
    }

    public String readData() {
        String readData = "";

        try {
            readData = fileIn.readLine();
        } catch (IOException ex) {
            System.out.println("*** Cannot Read File ***");
        }

        return readData;
    }

    public void writeData(String data) {
        fileOut.println(data);
    }

    public void closeInputFile() {
        try {
            fileIn.close();
        } catch (IOException ex) {
            System.out.println("*** Cannot Close File ***");
        }
    }

    public void closeOutputFile() {
        fileOut.close();
    }
}