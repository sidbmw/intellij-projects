import java.util.InputMismatchException;
import java.util.Scanner;

public class RPS2 {

    public static Scanner sc;
    public static int playerScore;
    public static int compScore;
    public static int gameCount;
    public static int numRounds;
    public static int startRounds;
    public static int playerPercent;
    public static String user;
    public static String comp;
    public static String playerName;
    public static IO file;
    public static final String FILE_NAME = "hiScores.txt";

    public static void main(String[] args) {
        boolean playAgain = true;

        file = new IO();
        file.createFile(FILE_NAME); //CREATE SAVE FILE

        //PLAY NEW GAME UNTIL PLAYER DOES NOT
        while (playAgain != false) {
            initGame();
            playAgain = runGame();
        }

        file.closeOutputFile();
        System.out.println("THANK YOU FOR PLAYING!");
    }

    public static void initGame() {
        sc = new Scanner(System.in);
        playerScore = 0;
        compScore = 0;
        gameCount = 0;
        numRounds = 0;
        playerName = "";

        user = "";
        comp = "";
    }

    public static boolean runGame() {

        playerName = getPlayerName();
        numRounds = getRounds();
        startRounds = numRounds; //SAVE ROUNDS FOR LATER

        while (numRounds > 0) {
            user = userChoice();
            comp = compChoice();
            determineWinner();
            numRounds--;
        }

        playerPercent = (int) (100 * ((float) playerScore / (float) startRounds));
        updateHiScores(); //UPDATE HIGH SCORES LIST

        return playAgain();
    }

    public static int getRounds() {
        int rounds = 0;

        while (rounds <= 0) {
            System.out.println("How many rounds would you like to play?");
            try {
                rounds = sc.nextInt();
            } catch (InputMismatchException ex) {
                System.out.println("Please input a number...");
                sc.nextLine(); //FLUSH BUFFER
            }
        }

        sc.nextLine(); //FLUSH BUFFER

        return rounds;
    }

    public static String getPlayerName() {
        String name = "";

        while (name == "") {
            System.out.println("What is your name?");
            name = sc.nextLine();
        }

        return name;
    }

    public static String userChoice() {
        String userChoice = "";

        do {
            System.out.println("Choose your weapon (\u001B[1mPaper, Scissors or Rock)\033[0m"); //BOLD FEATURE
            userChoice = sc.nextLine();
        } while (isValidChoice(userChoice) == false);

        return userChoice;
    }

    public static boolean isValidChoice(String choice) {
        boolean check = false;

        if (choice.equalsIgnoreCase("ROCK")) {
            check = true;
        } else if (choice.equalsIgnoreCase("PAPER")) {
            check = true;
        } else if (choice.equalsIgnoreCase("SCISSORS")) {
            check = true;
        } else {
            System.out.println("Make sure you enter either 'Rock', 'Paper' or 'Scissors'");
        }

        return check;
    }

    public static int getRandomInt(int n1, int n2) {
        int diff = Math.abs(n1 - n2) + 1;
        int lower = 0;
        double temp = (Math.random() * (double) diff);

        if (n1 <= n2) {
            lower = n1;
        } else {
            lower = n2;
        }

        return ((int) ((double) lower + temp));
    }

    public static String compChoice() {

        String computer = "";
        int compChoice = getRandomInt(1, 3);

        if (compChoice == 1) {
            computer = "ROCK";
        } else if (compChoice == 2) {
            computer = "PAPER";
        } else if (compChoice == 3) {
            computer = "SCISSORS";
        } else {
            computer = "ROCK";
        }

        return computer;
    }

    public static void determineWinner() {
        gameCount++;

        System.out.println("Computer Choice: " + comp);
        System.out.println("Your Choice: " + user.toUpperCase());

        if (comp.equalsIgnoreCase("ROCK") && user.equalsIgnoreCase("SCISSORS")) {
            computerWins();
        } else if (comp.equalsIgnoreCase("SCISSORS") && user.equalsIgnoreCase("PAPER")) {
            computerWins();
        } else if (comp.equalsIgnoreCase("PAPER") && user.equalsIgnoreCase("ROCK")) {
            computerWins();
        } else if (comp.equalsIgnoreCase(user.toString())) {
            tie();
        } else {
            playerWins(); //PLAYER WINS IF COMPUTER DOES NOT OR NOT TIE
        }

        System.out.println("We have played " + gameCount + " time/s. Your score is " + playerScore + " and the computers score is " + compScore);
        System.out.println("\n-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=--=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-\n");
    }

    public static void computerWins() {
        System.out.println("Computer wins! Better luck next time!");
        compScore++;
    }

    public static void playerWins() {
        System.out.println("You win!");
        playerScore++;
    }

    public static void tie() {
        System.out.println("Tie! the game must be played again.");
    }

    public static boolean playAgain() {
        System.out.println("Play again?(y/n)");
        String input = sc.nextLine();
        int check = 0;
        boolean result = false;

        while (check != 1) {
            if (input.equalsIgnoreCase("y")) {
                result = true;
                check = 1;
            } else if (input.equalsIgnoreCase("n")) {
                result = false;
                check = 1;
            } else {
                check = 0; //I WANT A VALID INPUT
            }
        }

        return result;
    }

    public static void updateHiScores() {

        String[] names = {"John", "Tammy", "Jan", "Fred", "Sue"};
        int[] scores = {10, 20, 30, 40, 33};

        int i = 0;
        int temp = 0;
        String tempString = "";

        file.openFile(FILE_NAME);

        //check player score
        for (i = 0; i < 5; i++) {
            if (playerPercent > scores[i]) {
                scores[i] = playerPercent;
                names[i] = playerName;
                break;
            }
        }

        //sorting algorithm
        for (i = 0; i < scores.length; i++) {
            for (int j = 0; j < scores.length - 1; j++) {
                if (scores[j] < scores[j + 1]) {
                    temp = scores[j];
                    scores[j] = scores[j + 1];
                    scores[j + 1] = temp;

                    tempString = names[j];
                    names[j] = names[j + 1];
                    names[j + 1] = tempString;
                }
            }
        }

        //write updated hi score data
        for (i = 0; i < 5; i++) {
            file.writeData(names[i].toString() + " " + scores[i] + "%");
        }

        file.closeInputFile();
    }
}