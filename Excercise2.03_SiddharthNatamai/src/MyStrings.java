/**
 * @Author: Siddharth Natamai
 * @Date: Nov 5 2017
 * @Description: Collection of methods to manipulate strings
 */
public class MyStrings {

    /**
     * @param americanDate Changing date format to american standard
     * @return Return new formatted date
     */
    public static final String convertDate(final String americanDate) {
        final String[] parts = americanDate.split("/");
        if (parts.length != 3) {
            throw new IllegalArgumentException("Expected date in format mm/dd/yyyy");
        }
        return parts[1] + "-" + parts[0] + "-" + parts[2];
    }

    /**
     * @param phoneNum Changing phone number input to a new format
     * @return Return new formatted phone number
     */
    public static final long convertPhoneNum(final String phoneNum) {
        try {
            return Long.parseLong(phoneNum.replaceAll("[^0-9]", ""));
        } catch (final NumberFormatException check) {
            throw new IllegalArgumentException("Expected phone number in formats like (xxx)xxx-xxxx", check);
        }
    }


    /**
     * @param name Splitting input at "" and rearranging it to LastName, FirstName
     * @return Return name after formatting
     */
    public static final String convertName(final String name) {
        String[] sArray = name.split("\\s{2,}");

        return sArray[1] + ", " + sArray[0];
    }

    /**
     * @param original Original string input is reversed
     * @return New reversed string is returned
     */
    public static final String reverseString(final String original) {
        String reverse = "";
        int length = original.length();

        for (int i = length - 1; i >= 0; i--)
            reverse = reverse + original.charAt(i);

        return new String(reverse);
    }

    /**
     * @param a String a
     * @param b String b
     * @return String a and String b is compared to check for a anagram and a boolean is returned
     */
    static boolean areAnagrams(String a, String b) {
        char[] balance1 = a.toLowerCase().toCharArray(), balance2 = b.toLowerCase().toCharArray();
        if (balance1.length != balance2.length)
            return false;
        int[] counts = new int[26];
        for (int i = 0; i < balance1.length; i++) {
            counts[balance1[i] - 97]++;
            counts[balance2[i] - 97]--;
        }

        for (int i = 0; i < 26; i++) {
            if (counts[i] != 0) {
                return false;
            }
        }
        return true;
    }

}