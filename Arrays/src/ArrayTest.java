/**
 * A few examples of how to declare, initialize and work with an
 * array in Java.
 * <p>
 * ARRAY = LIST of elements
 *
 * @author Ms Cianci
 * @modified Mr. Roller May 6, 2016
 * @since Monday, November 19, 2012
 */
public class ArrayTest {
    public static void main(String[] args) {
        String name = "Alex";    // name can only store one string
        String[] names = {"Alex", "Sophie", "James", "Jeff", "Jeff"};
        int[] evenNums = {2, 4, 6, 8, 10, 12};
        int[] randArray = new int[30];

        System.out.println(name.length()); // 4 <- length of "Alex"
        System.out.println(names.length);  // 5 <- number of names in the list
        System.out.println(names[1].length());  // 6 <- length of "Sophie"

        System.out.println(name);
        System.out.println(names);  // quite useless...!

        // use a for loop to access every element:
        for (int i = 0; i < names.length; i++) {
            System.out.println(names[i].toUpperCase());
        }

        for (int k = 0; k < evenNums.length; k++)
            System.out.print(evenNums[k] + " ");
        System.out.println();

        String[] classList = new String[18]; //filled with nulls to start
        classList[0] = "Orsi";
        classList[1] = "Sophie";
        classList[2] = "Oliver";

        // To capture the last defined element in the array...
        for (int i = 0; i < classList.length && classList[i] != null; i++)
            System.out.println(classList[i]);


        // The following will cause a run-time error because it will attempt
        // to call
        // upon the toUpperCase method for a null element
        //for(int i = 0; i < classList.length; i++)
        //{
        //System.out.println(classList[i].toUpperCase());
        // causes a run-time error when it reaches the first 'null'
        //}

        System.out.println(names[0] + " " + classList[0]);

        //this for loop displays 30 random numbers between
        //1 and 10
        for (int i = 0; i < 30; i++) {
            randArray[i] = (int) (Math.random() * 10) + 1;
            System.out.print(randArray[i] + " ");
        }
    } // end main method
} // end class'