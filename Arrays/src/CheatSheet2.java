import java.util.*; //VERY IMPORTANT FOR AN ARRAYS

public class CheatSheet2 {
    public static void main(String[] args) {
        //Simple Array
        String[] array = new String[]{"John", "Mary", "Bob"};
        System.out.println(Arrays.toString(array));
        //Output
        //[John, Mary, Bob]


        //Nested Array
        String[][] deepArray = new String[][]{{"John", "Mary"}, {"Alice", "Bob"}};
        System.out.println(Arrays.toString(deepArray));
        //output: [[Ljava.lang.String;@106d69c, [Ljava.lang.String;@52e922]
        System.out.println(Arrays.deepToString(deepArray));
        //Output
        //[[John, Mary], [Alice, Bob]]

        //Double Array
        double[] doubleArray = {7.0, 9.0, 5.0, 1.0, 3.0};
        System.out.println(Arrays.toString(doubleArray));
        //Output
        //[7.0, 9.0, 5.0, 1.0, 3.0 ]


        //Int Array
        int[] intArray = {7, 9, 5, 1, 3};
        System.out.println(Arrays.toString(intArray));
        //Output
        //[7, 9, 5, 1, 3 ]
    }
}