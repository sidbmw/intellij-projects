import java.util.*;

public class ArrayListCheatSheet {

    //The issue with arrays is that they are of fixed length so if it is full you cannot add any more elements to it,
    // likewise if there are number of elements gets removed from it the memory consumption would be the same as it
    // doesn’t shrink. On the other ArrayList can dynamically grow and shrink after addition and removal of elements.
    // Apart from these benefits ArrayList class enables us to use predefined methods of it which makes our task easy.


    public static void main(String args[]) {
        /*Creation of ArrayList: I'm going to add String
         *elements so I made it of string type */
        ArrayList<String> obj = new ArrayList<String>();

        /*This is how elements should be added to the array list*/
        obj.add("Ajeet");
        obj.add("Harry");
        obj.add("Chaitanya");
        obj.add("Steve");
        obj.add("Anuj");

        /* Displaying array list elements */
        System.out.println("Currently the array list has following elements:" + obj);

        /*Add element at the given index*/
        obj.add(0, "Rahul");
        obj.add(1, "Justin");

        /*Remove elements from array list like this*/
        obj.remove("Chaitanya");
        obj.remove("Harry");

        System.out.println("Current array list is:" + obj);

        /*Remove element from the given index*/
        obj.remove(1);

        System.out.println("Current array list is:" + obj);

    }
}

//Output:
//Currently the array list has following elements:[Ajeet, Harry, Chaitanya, Steve, Anuj]
//Current array list is:[Rahul, Justin, Ajeet, Steve, Anuj]
//Current array list is:[Rahul, Ajeet, Steve, Anuj]