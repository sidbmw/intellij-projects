import java.util.Scanner;

public class Algorithm {
    public static void main(String[] args) {

        String playAgain = "n";
        Scanner sc = new Scanner(System.in);
        System.out.print("Please enter a integer between 2 and 100: ");
        do {

            while (!sc.hasNextInt()) {
                sc.next();
                System.out.print("Please enter a integer between 2 and 100: ");
            }

            int userInput = sc.nextInt();

            if (userInput >= 2 && userInput <= 100) {

                System.out.print(userInput + " ");

                do {
                    if (userInput % 2 == 0) {
                        userInput = userInput / 2;
                        System.out.print(userInput + " ");

                    } else {
                        userInput = userInput * 3 + 1;
                        System.out.print(userInput + " ");
                    }

                } while (userInput != 1);

                sc.nextLine();//flush buffer
                System.out.println("\n1");
                System.out.println("\nWould you like to play again? <Y or y>");
                playAgain = sc.nextLine();
                System.out.println("2");
            }


        } while (playAgain.equalsIgnoreCase("y"));
        System.out.println("Thanks for playing, hope to see you again soon!");
    }
}
