import java.util.Scanner;

public class test {
    public static void main(String[] args) {

        String playAgain = "n";
        Scanner sc = new Scanner(System.in);

        do {

            do {
                System.out.print("Please enter a integer between 2 and 100: ");
            } while (!sc.hasNextInt());

            int userInput = sc.nextInt();

            if (userInput >= 2 && userInput <= 100) {

                System.out.print(userInput + " ");

                do {
                    if (userInput % 2 == 0) {
                        userInput = userInput / 2;
                        System.out.print(userInput + " ");

                    } else {
                        userInput = userInput * 3 + 1;
                        System.out.print(userInput + " ");
                    }

                } while (userInput != 1);

                sc.nextLine();//flush buffer
                System.out.println("Would you like to play again? <Y or y>");
                playAgain = sc.nextLine();
            }


        } while (playAgain.compareTo("y") == 0 || playAgain.compareTo("Y") == 0);
        System.out.println("Thanks for playing, hope to see you again soon!");
    }
}
