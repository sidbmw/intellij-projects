import java.text.DecimalFormat;
import java.util.Scanner;
import java.math.*;

/*

    Description: Program that uses methods to solve:

        a) right angle triangle given the lengths of 2 sides
        b) right angle triangle given one side and an angle
        c) non-right triangle given two sides and the angle between them
        d) non-right triangle given all three sides
        e) non-right triangle given two angles and a side
        f) non-right triangle given two sides and the angle that is not between them

    Author: Siddharth Natamai
    Date: December 16, 2017
 */

public class Trignometry {
    public static void main(String[] args) {
        String playAgain = "n";
        do {
            Scanner sc = new Scanner(System.in);
            System.out.println("Choose either: a, b, c, d, e, f\n");
            System.out.println("        a) right angle triangle given the lengths of 2 sides\n" +
                    "        b) right angle triangle given one side and an angle\n" +
                    "        c) non-right triangle given two sides and the angle between them\n" +
                    "        d) non-right triangle given all three sides\n" +
                    "        e) non-right triangle given two angles and a side\n" +
                    "        f) non-right triangle given two sides and the angle that is not between them");
            String userInput = sc.nextLine();

            if (userInput.equalsIgnoreCase("a")) {
                System.out.print("Which 2 sides relative to a targeted angle would you like to provide? (1 - opp & hyp, 2 - adj & hyp, 3 - opp & adj): ");
                int userInput2 = sc.nextInt();

                if (userInput2 == 1) {
                    System.out.println("Enter the length of the opposite side: ");
                    double input1 = sc.nextDouble();
                    System.out.println("Enter the length of the hypotenuse: ");
                    double input2 = sc.nextDouble();

                    double adj = oppHypA(input1, input2);
                    DecimalFormat numberFormat = new DecimalFormat("#.000");
                    System.out.println("Your Triangle has sides: " + input1 + ", " + input2 + ", " + adj);
                    double angle1 = Math.toDegrees(Math.asin(input1 / input2));
                    double angle2 = 90;
                    double angle3 = 180 - angle1 - angle2;
                    System.out.println("Triangle has angles " + numberFormat.format(angle1) + " " + angle2 + " " + angle3);
                }

                if (userInput2 == 2) {
                    System.out.println("Enter the length of the adj side: ");
                    double input1 = sc.nextDouble();
                    System.out.println("Enter the length of the hypotenuse: ");
                    double input2 = sc.nextDouble();

                    double returned = adjHypA(input1, input2);
                    DecimalFormat numberFormat = new DecimalFormat("#.000");
                    System.out.println("Your Triangle has sides: " + input1 + ", " + input2 + ", " + returned);
                    double angle1 = Math.toDegrees(Math.acos(input1 / input2));
                    double angle2 = 90;
                    double angle3 = 180 - angle1 - angle2;
                    System.out.println("Triangle has angles " + numberFormat.format(angle1) + " " + angle2 + " " + angle3);
                }

                if (userInput2 == 3) {
                    System.out.println("Enter the length of the opp side: ");
                    double input1 = sc.nextDouble();
                    System.out.println("Enter the length of the adj: ");
                    double input2 = sc.nextDouble();

                    double returned = oppAdj(input1, input2);
                    DecimalFormat numberFormat = new DecimalFormat("#.000");
                    System.out.println("Your Triangle has sides: " + input1 + ", " + input2 + ", " + returned);
                    double angle1 = Math.toDegrees(Math.atan(input1 / input2));
                    double angle2 = getAngle(input1, input2, returned);
                    double angle3 = 180 - angle1 - angle2;
                    System.out.println("Triangle has angles " + numberFormat.format(angle1) + " " + angle2 + " " + angle3);
                }
            }


            if (userInput.equalsIgnoreCase("b")) {
                System.out.println("Which side length would you like to enter? (1 - hypotenuse, 2 - opposite, 3 - adjacent)");
                int userInput2 = sc.nextInt();

                if (userInput2 == 1) {
                    System.out.println("Enter the length of the hypotenuse");
                    double hyp = sc.nextDouble();
                    System.out.println("Enter a angle");
                    double angle = sc.nextDouble();

                    while (angle > 90) {
                        System.out.println("Invalid angle, please enter a angle again");
                        angle = sc.nextDouble();
                    }

                    double angle2 = 90;
                    double angle3 = 180 - angle - angle2;

                    double opp = oppLength(hyp, angle);
                    double adj = adjLength(hyp, angle3);

                    System.out.println("Length of opposite is: " + opp + ". Length of adjacent is: " + adj + ". Length of hypotenuse is: " + hyp + ".");
                    System.out.println("Triangle has angles " + angle + " " + angle2 + " " + angle3);
                }

                if (userInput2 == 2) {
                    System.out.println("Enter the length of the opposite side");
                    double opp = sc.nextDouble();
                    System.out.println("Enter a angle");
                    double angle = sc.nextDouble();

                    while (angle > 90) {
                        System.out.println("Invalid angle, please enter a angle again");
                        angle = sc.nextDouble();
                    }

                    double adjOppAngle = 90;
                    double adjHypAngle = angle;
                    double oppHypAngle = angles(angle);
                    System.out.println(oppHypAngle);

                    double oppLength = opp;
                    double hypLength = hypLength(opp, angle);
                    double adjLength = oppHypA(oppLength, hypLength);
                    System.out.println("Length of opposite is: " + oppLength + ". Length of adjacent is: " + adjLength + ". Length of hypotenuse is: " + hypLength + ".");
                    System.out.println("Triangle has angles " + adjHypAngle + " " + adjOppAngle + " " + oppHypAngle);
                }

                if (userInput2 == 3) {
                    System.out.println("Enter the length of the adjacent side");
                    double adj = sc.nextDouble();
                    System.out.println("Enter a angle");
                    double angle = sc.nextDouble();

                    while (angle > 90) {
                        System.out.println("Invalid angle, please enter a angle again");
                        angle = sc.nextDouble();
                    }

                    double adjOppAngle = 90;
                    double adjHypAngle = angle;
                    double oppHypAngle = angles(angle);
                    System.out.println(oppHypAngle);

                    double adjLength = adj;
                    double oppLength = oppAdjB(adj, angle);
                    double hypLength = oppAdj(adjLength, oppLength);
                    System.out.println("Length of opposite is: " + oppLength + ". Length of adjacent is: " + adjLength + ". Length of hypotenuse is: " + hypLength + ".");
                    System.out.println("Triangle has angles " + adjOppAngle + " " + adjHypAngle + " " + oppHypAngle);
                }
            }


            if (userInput.equalsIgnoreCase("c")) {
                System.out.println("Enter a length");
                double length1 = sc.nextDouble();
                System.out.println("Enter another length");
                double length2 = sc.nextDouble();
                System.out.println("Enter a angle");
                double angle1 = sc.nextDouble();
                while (angle1 > 180) {
                    System.out.println("Invalid angle, please enter a angle again");
                    angle1 = sc.nextDouble();
                }
                System.out.println("Enter another angle");
                double angle2 = sc.nextDouble();
                while (angle2 > 180) {
                    System.out.println("Invalid angle, please enter a angle again");
                    angle2 = sc.nextDouble();
                }
                double angle3 = 180 - angle1 - angle2;

                double length3 = getLengthC(length1, length2, angle3);
                System.out.println("Triangle has lengths: " + length1 + " " + length2 + " " + length3);
                System.out.println("Triangle has angles: " + angle1 + " " + angle2 + " " + angle3);
            }

            if (userInput.equalsIgnoreCase("d")) {
                System.out.println("Enter length one");
                double length1 = sc.nextDouble();
                System.out.println("Enter length two");
                double length2 = sc.nextDouble();
                System.out.println("Enter length three");
                double length3 = sc.nextDouble();

                double angle1 = getAngle(length1, length2, length3);
                double angle2 = getAngle(length2, length3, length1);
                double angle3 = 180 - angle1 - angle2;
                angle3 = getRounded(angle3, 3);
                System.out.println("Triangle has lengths: " + length1 + " " + length2 + " " + length3);
                System.out.println("Triangle has angles: " + angle1 + " " + angle2 + " " + angle3);
            }

            if (userInput.equalsIgnoreCase("e")) {
                System.out.println("Enter angle");
                double angle1 = sc.nextDouble();
                while (angle1 > 180) {
                    System.out.println("Invalid angle, please enter a angle again");
                    angle1 = sc.nextDouble();
                }
                System.out.println("Enter another angle");
                double angle2 = sc.nextDouble();
                while (angle2 > 180) {
                    System.out.println("Invalid angle, please enter a angle again");
                    angle2 = sc.nextDouble();
                }
                double angle3 = 180 - angle1 - angle2;
                System.out.println("Enter a length");
                double length = sc.nextDouble();
                double length2 = getLengthE(length, angle2, angle3);
                double length3 = getLengthE2(length2, angle3, angle2);
                System.out.println("Triangle has lengths: " + length + " " + length2 + " " + length3);
                System.out.println("Triangle has angles: " + angle1 + " " + angle2 + " " + angle3);
            }

            if (userInput.equalsIgnoreCase("f")) {
                System.out.println("Enter a length of first side");
                double length1 = sc.nextDouble();
                System.out.println("Enter length of second side");
                double length2 = sc.nextDouble();
                System.out.println("Enter angle");
                double angle1 = sc.nextDouble();
                while (angle1 > 180) {
                    System.out.println("Invalid angle, please enter a angle again");
                    angle1 = sc.nextDouble();
                }

                double length3 = getLengthC(length1, length2, angle1);
                double angle2 = getAngle(length1, length2, length3);
                double angle3 = 180 - angle1 - angle2;
                angle3 = getRounded(angle3, 3);
                System.out.println("Triangle has lengths: " + length1 + " " + length2 + " " + length3);
                System.out.println("Triangle has angles: " + angle1 + " " + angle2 + " " + angle3);

            } else {
                System.exit(0);
            }

            System.out.println("Enter y to play again");
            playAgain = sc.next();
        } while (playAgain.equalsIgnoreCase("y"));
    }


    public static double oppHypA(double input1, double input2) {
        double aSquare = input1;
        double cSquare = input2;
        double bSquared = Math.pow(cSquare, 2) - Math.pow(aSquare, 2);
        bSquared = Math.sqrt(bSquared);

        return getRounded(bSquared, 3);
    }

    public static double adjHypA(double input1, double input2) {
        double aSquare = input1;
        double cSquare = input2;
        double bSquared = Math.pow(cSquare, 2) - Math.pow(aSquare, 2);
        bSquared = Math.sqrt(bSquared);

        return getRounded(bSquared, 3);
    }

    public static double oppAdj(double input1, double input2) {
        double aSquare = input1;
        double cSquare = input2;
        double bSquared = Math.pow(aSquare, 2) + Math.pow(cSquare, 2);
        bSquared = Math.sqrt(bSquared);

        return getRounded(bSquared, 3);
    }

    public static double angles(double input1) {
        double angle1 = 90;
        double angle2 = input1;
        double angle3 = 180 - angle1 - angle2;

        return getRounded(angle3, 3);
    }

    public static double oppLength(double hypotenuse, double angle) {
        double hyp = hypotenuse;
        double opp = Math.sin(angle) * hyp;
        return getRounded(opp, 3);
    }

    public static double adjLength(double hypotenuse, double angle2) {
        double hyp = hypotenuse;
        double adj = Math.cos(angle2) * hyp;

        return getRounded(adj, 3);
    }

    public static double hypLength(double opp, double angle) {
        double hyp = opp / Math.sin(angle);
        return getRounded(hyp, 3);
    }

    public static double oppAdjB(double adj, double angle) {
        double oppLength = Math.tan(angle) * adj;
        return getRounded(oppLength, 3);
    }

    public static double getLengthC(double length1, double length2, double angle3) {
        double length3 = Math.pow(length1, 2) + Math.pow(length2, 2) - (2 * length1) + (2 * length2) * Math.cos(angle3);
        length3 = Math.sqrt(length3);
        return getRounded(length3, 3);
    }

    public static double getAngle(double length1, double length2, double length3) {
        double angle = Math.acos((Math.pow(length1, 2) + Math.pow(length2, 2) - Math.pow(length3, 2)) / (2 * length1 * length2));
        angle = Math.toDegrees(angle);
        angle = getRounded(angle, 3);
        return getRounded(angle, 3);
    }

    public static double getLengthE(double length, double angle, double angle2) {
        double lengthX = (length * Math.sin(angle)) / Math.sin(angle2);
        return getRounded(lengthX, 3);
    }

    public static double getLengthE2(double length, double angle, double angle2) {
        double lengthX = (length * Math.sin(angle)) / Math.sin(angle2);
        return getRounded(lengthX, 3);
    }


    public static double getRounded(double n, int d) {
        double temp = (n * Math.pow(10, d) + 0.5);
        int temp2 = (int) temp;
        double temp3 = (double) temp2 / Math.pow(10, d);
        return (temp3);
    }
}