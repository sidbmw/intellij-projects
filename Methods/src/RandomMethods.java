import java.util.Scanner;

/*

    Description: Random useful methods

    Author: Siddharth Natamai
    Date: December 11, 2017
 */


public class RandomMethods {
    public static void main(String[] args) {
        String playAgain = "n";

        do {

            Scanner sc = new Scanner(System.in);
            System.out.println("Choose a option from 1-4: ");
            int userInput = sc.nextInt();

            if (userInput == 1) {
                System.out.println("Enter a minimum int value: ");
                int num1 = sc.nextInt();
                System.out.println("Enter a maximum int value: ");
                int num2 = sc.nextInt();

                while (num1 >= num2) {
                    System.out.println("Enter minimum int value: ");
                    num1 = sc.nextInt();
                    System.out.println("Enter a maximum int value: ");
                    num2 = sc.nextInt();
                }
                int randomInt = getRandomInt(num1, num2);
                System.out.println(randomInt);
            }

            if (userInput == 2) {
                System.out.println("Enter a minimum double value: ");
                int num1 = sc.nextInt();
                System.out.println("Enter a maximum double value: ");
                int num2 = sc.nextInt();

                while (num1 >= num2) {
                    System.out.println("Enter minimum double value: ");
                    num1 = sc.nextInt();
                    System.out.println("Enter a maximum double value: ");
                    num2 = sc.nextInt();
                }
                double randomDouble = getRandomDouble(num1, num2);
                System.out.println(randomDouble);
            }

            if (userInput == 3) {
                String flippedCoin = getFlippedCoin();
                System.out.println(flippedCoin);
            }

            if (userInput == 4) {
                char randomChar = generateRandomChar();
                System.out.print("Random char is: ");
                System.out.print(randomChar + "\n");
            }

            System.out.println("Do you want to play again?");
            playAgain = sc.next();
        } while (playAgain.equalsIgnoreCase("y"));
        System.out.println("Thank you, Have a nice day!");
    }


    public static int getRandomInt(int n1, int n2) {
        int diff = Math.abs(n1 - n2) + 1;
        int lower = 0;
        double temp = (Math.random() * (double) diff);
        if (n1 <= n2)
            lower = n1;
        else
            lower = n2;

        return ((int) ((double) lower + temp));
    }

    public static double getRandomDouble(int n1, int n2) {
        int diff = Math.abs(n1 - n2);
        int lower = 0;
        double temp = (Math.random() * (double) diff);
        if (n1 <= n2)
            lower = n1;
        else
            lower = n2;

        return ((double) lower + temp);
    }

    public static String getFlippedCoin() {
        String strFlipped = "";
        String heads = "H ";
        String tails = "T ";

        for (int i = 0; i < 10; i++) {
            int randomInt = getRandomInt(0, 2147483646);
            if (randomInt % 2 == 0) {
                strFlipped = strFlipped + heads;
            }

            if (randomInt % 2 == 1) {
                strFlipped = strFlipped + tails;
            }
        }

        return strFlipped;
    }

    public static char generateRandomChar() {
        String alphabets = "aAbBcCdDeEfFgGhHiIjJkKlLmMnNoOpPqQrRsStTuUvVwWxXyYzZ";
        int randNum = getRandomInt(0, 51);

        return alphabets.charAt(randNum);
    }
}