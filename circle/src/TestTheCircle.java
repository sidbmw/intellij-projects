/**
 * @author Mrs. Cullum  27 October 2015
 * This class tests the MyCircle class
 */
public class TestTheCircle {

    public static void main(String[] args) {

        //print the value of pi
        System.out.println("The value of pi is " + MyCircle.PI);


        //create circles with each of the types of constructors
        MyCircle circle1 = new MyCircle(3.42, "cm");
        MyCircle circle2 = new MyCircle(2.44);
        MyCircle circle3 = new MyCircle();

        //tests toString() and the constructors
        System.out.println(circle1.toString());
        System.out.println(circle2.toString());
        System.out.println(circle3.toString());

        //show that you can't print the private variables
        //System.out.println("I cannot access the radius directly - " + circle1.radius);

        //tests equals() - expect to return false
        System.out.println("Is circle1 equal to circle2? " + circle1.equals(circle2));
        //set circle2 radius to 3.42. set circle2 units to cm
        circle2.setRadius(3.42);
        circle2.setUnits("cm");
        System.out.println("Circle2 radius is now " + circle2.getRadius());
        System.out.println("Circle2 units is now " + circle2.getUnits());

        //tests equals() - expect to return true
        System.out.println("Is circle1 equal to circle2? " + circle1.equals(circle2));

        //use instance methods to calculate the circumference and area.
        System.out.println("Circumference of circle 3: " + circle3.getCircumference());
        System.out.println("Area of circle 3: " + circle3.getArea());


        //use static methods to calculate the circumference and area.
        System.out.println("Using static methods");
        System.out.println("Circumference of a circle with radius 1.23: " + MyCircle.getCircumference(1.23));
        System.out.println("Area of a circle with radius 4.443: " + MyCircle.getArea(4.443));


    }

}