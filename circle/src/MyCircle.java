/**
 * @author Mrs. Cullum October 27, 2015
 * @modifications Mr. Roller April 2, 2016
 * MyCircle class, as defined in the UML diagram
 */
public class MyCircle {

    //instance variables
    private double radius;
    private String units;

    //static variables
    public final static double PI = Math.PI;
    private static int numCircles = 0;

    /**
     * Constructor method
     *
     * @param r represents radius
     * @param u represents units of the radius
     */
    public MyCircle(double r, String u) {

        //initialize instance variables
        if (r > 0) {
            radius = r;
        } else {
            radius = 1;
        }
        units = u;

        //increments circle count
        numCircles++;

    }

    /**
     * Constructor method with 1 parameter
     *
     * @param r represents radius
     */
    public MyCircle(double r) {

        //sets units to "units"
        this(r, "units");
    }

    /**
     * Parameterless constructor method
     */
    public MyCircle() {

        //sets the radius = 1, units to "units"
        this(1, "units");
    }

    /**
     * setRadius() sets the value of the instance variable radius
     *
     * @param r is the new radius
     */
    public void setRadius(double r) {

        if (r > 0) {
            radius = r;
        }

    }

    /**
     * setUnits() sets the value of the instance variable units
     *
     * @param s is the new value of unit
     */
    public void setUnits(String s) {

        units = s;
    }

    /**
     * getRadius() returns the value of the instance variable radius
     *
     * @return the radius of the circle
     */
    public double getRadius() {

        return radius;
    }

    /**
     * getDiameter() returns the diameter of the circle
     *
     * @return a double, the diameter of the circle
     */
    public double getDiameter() {

        return (radius * 2);
    }

    /**
     * getUnits() returns the value of the instance variable radius
     *
     * @return a string, representing the units of the circle's radius
     */
    public String getUnits() {

        return units;
    }

    /**
     * getNumCircles() is a static method that returns the number of circle objects
     *
     * @return an int representing the number of circles created
     */
    public static int getNumCircles() {

        return numCircles;
    }

    /**
     * getCircumference() returns the circumference of the circle object
     *
     * @return a double representing the circumference of the circle
     */
    public double getCircumference() {

        double value;
        value = 2 * PI * radius;
        return value;
    }

    /**
     * getCircumference() is an overloaded method.
     * This method is static. This means that it is called on the class, not the object
     *
     * @param r, a double which represents the radius of the circle
     * @return a double representing the circumference of the circle with that radius
     * returns -1 if input is bad
     */
    public static double getCircumference(double r) {

        double value;

        if (r > 0) {
            value = 2 * PI * r;
        } else {
            value = -1;
        }
        return value;
    }

    /**
     * getArea() returns the area of the circle object
     *
     * @return a double representing the area of the circle
     */
    public double getArea() {

        double value;
        value = PI * Math.pow(radius, 2);
        return value;
    }

    /**
     * getCircumference() is an overloaded method.
     * This method is static. This means that it is called on the class, not the object
     *
     * @param r, a double which represents the radius of the circle
     * @return a double representing the area of the circle with that radius
     * returns -1 if input is bad
     */
    public static double getArea(double r) {

        double value;

        if (r > 0) {
            value = PI * Math.pow(r, 2);
        } else {
            value = -1;
        }
        return value;
    }

    /**
     * equals() checks to see if 2 objects are equal. This can only be true
     * if the two objects are both MyCircle objects
     *
     * @param Object
     * @return boolean value of true if the objects are equal and false otherwise
     * Note: When our object was passed to the method we didn't know what type
     * it was, & even though we are now sure it is of type MyCircle inside the
     * first if statement, it still needs to be cast as a MyCircle object so
     * we can use it as such
     */
    public boolean equals(Object c) {

        boolean flag = false;

        if (c instanceof MyCircle) {

            if (((MyCircle) c).radius == this.radius) {
                flag = true;
            }
        }
        return flag;
    }

    //Note: the following method is how we created our equal
    //over-ride method in our Rectangle class.  It works
    //just as well, but counts on the fact that you already know
    //the objects are instances of the same class. (Therefore
    //it is less generic than the code above...
    /* public boolean equals(MyCircle circ){
		return(radius==circ.radius);
	} */

    /**
     * toString() creates a string representation of the object
     *
     * @return a string including the radius and units
     */
    public String toString() {

        String returnString;
        returnString = "This MyCircle object has a radius of " + radius + " " + units;
        return returnString;
    }
}