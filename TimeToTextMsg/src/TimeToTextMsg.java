import java.util.Scanner;
import java.util.function.Function;


public class TimeToTextMsg {

    @SuppressWarnings("PointlessArithmeticExpression")
    private static final class PhoneNumberTyper {

        private static final String[] dataMap;

        static {

            dataMap = new String['z' - 'a' + 1];
            dataMap['a' - 'a'] = "2";
            dataMap['b' - 'a'] = "22";
            dataMap['c' - 'a'] = "222";
            dataMap['d' - 'a'] = "3";
            dataMap['e' - 'a'] = "33";
            dataMap['f' - 'a'] = "333";
            dataMap['g' - 'a'] = "4";
            dataMap['h' - 'a'] = "44";
            dataMap['i' - 'a'] = "444";
            dataMap['j' - 'a'] = "5";
            dataMap['k' - 'a'] = "5";
            dataMap['l' - 'a'] = "555";
            dataMap['m' - 'a'] = "6";
            dataMap['n' - 'a'] = "66";
            dataMap['o' - 'a'] = "666";
            dataMap['p' - 'a'] = "7";
            dataMap['q' - 'a'] = "77";
            dataMap['r' - 'a'] = "777";
            dataMap['s' - 'a'] = "7777";
            dataMap['t' - 'a'] = "8";
            dataMap['u' - 'a'] = "88";
            dataMap['v' - 'a'] = "888";
            dataMap['w' - 'a'] = "9";
            dataMap['x' - 'a'] = "99";
            dataMap['y' - 'a'] = "999";
            dataMap['z' - 'a'] = "9999";
        }


        static final String getKeysForLetter(final int letter) {
            if ((letter < 'a') || (letter > 'z')) {
                throw new IllegalArgumentException("Letter does not appear on phone!");
            }


            return dataMap[letter - 'a'];
        }

        private PhoneNumberTyper() {
            throw new UnsupportedOperationException();
        }
    }

    public static void main(final String[] ignore) {
        System.out.println("GOOOOOOOOOOOOOO");
        @SuppressWarnings("resource") final Scanner scanner = new Scanner(System.in);

        while (true) {
            final String input = scanner.nextLine();

            if (input.equals("halt")) {
                return;
            }

            System.out.println(calculateTime(input));
        }
    }


    private static final class Mapper implements Function<String, String> {

        char lastKey = '\uFFFF';


        @Override
        public final String apply(String str) {

            if (lastKey == str.charAt(0)) {
                str = ".." + str;
            }

            lastKey = str.charAt(str.length() - 1);

            return str;
        }
    }


    private static int calculateTime(final String msg) {
        return msg.toLowerCase()
                .replaceAll("[^a-z]", "")
                .chars()
                .mapToObj(PhoneNumberTyper::getKeysForLetter)
                .map(new Mapper())
                .mapToInt(String::length)
                .sum();
    }
}
