public class User {

    public static void main(String[] args) {
        String string1 = "Gord Roller";
        String newString = "Roller Roller Roller";

        System.out.println("The full string name is...");
        System.out.println(string1);
        System.out.println("but if we remove the white space...");

        String string2 = StringFunctions.removewhiteSpace(string1);
        System.out.println(string2);

        //return first name from string1
        System.out.println("And just the first name is...");
        String string3 = StringFunctions.returnFirstName(string1);
        System.out.println(string3);

        //return the last name from string1
        System.out.println("And just the last name is...");
        String string4 = StringFunctions.returnLastName(string1);
        System.out.println(string4);

        //further demo capabilities of indexof() and lastIndexOf()
        System.out.println("Let's switch to a new string:  " + newString);
        String string5 = StringFunctions.returnLocation1(newString);
        System.out.println(string5);
        String string6 = StringFunctions.returnLocation2(newString);
        System.out.println(string6);
        String string7 = StringFunctions.returnLocation3(newString);
        System.out.println(string7);
    }
}