public class StringFunctions {

    /**
     * @param String
     * @return a new string with white space removed
     * Removes all white space */
    public static String removewhiteSpace(String s){
        String newString;
        newString = s.replaceAll(" ", "");
        return newString;
    }

	/*
	 * From a string of the format: FirstName LastName
	 * return just the first name.
	 *
	 * @param String
	 * @return a new string that is just the first name
	 *
	 */

    public static String returnFirstName(String s){

        String newString;
        //find the index of the delimiter " "
        int endOfString;
        endOfString = s.indexOf(" ");
        //find the substring that starts at 0 and ends with the delimiter.
        newString = s.substring(0,endOfString);
        return newString;
    }

    /*
     * From a string of the format: FirstName LastName
     * return "Mrs. LastName"
     *
     * @param String
     * @return a new string that is mrs. + last name
     */
    public static String returnLastName(String s){
        String newString;
        //find the index of the delimiter " "
        int delimiter;
        delimiter = s.indexOf(" ");
        //find the substring that starts at the delimiter to the end.
        newString = "Mr. " + s.substring(delimiter + 1);
        return newString;
    }

    //return string containing the first instance of "olle"
    public static String returnLocation1(String s){
        String newString;
        int delimiter;
        newString = "The first occurance of olle is found at location " + s.indexOf("olle");
        return newString;
    }

    //return string containing the second instance of "olle"
    public static String returnLocation2(String s){
        String newString;
        int delimiter;
        int spaceDelimeter;
        spaceDelimeter = s.indexOf(" ");
        newString = "And the second occurance of olle is found at location " + s.indexOf("olle", spaceDelimeter);
        return newString;
    }

    //return string containing the third instance of "olle"
    public static String returnLocation3(String s){
        String newString;
        int delimiter;
        newString = "And the last occurance of olle is found at location " + s.lastIndexOf("olle");
        return newString;
    }
}