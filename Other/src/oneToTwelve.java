public class oneToTwelve {
    public static void main(String[] args) {
        int mainNum = 1;

        for (int i = mainNum; i <= 12; i++) {
            if (i <= 4) {
                System.out.print(i);
                System.out.print("  ");

                if (i == 4){
                    System.out.println("");
                }
            }

            if (i > 4 && i < 9) {
                System.out.print(i);
                System.out.print("  ");

                if (i == 8){
                    System.out.println("");
                }
            }

            if (i > 8 && i <= 12) {
                System.out.print(i);
                System.out.print(" ");

                if (i == 12){
                    System.out.println("");
                }
            }
        }
    }
}
