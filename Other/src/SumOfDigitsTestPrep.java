public class SumOfDigitsTestPrep {
    public static void main(String[] args) {

        for (int i = 100; i > 99 && i < 1000; i++) {
            String strNum = Integer.toString(i);
            String digit1 = strNum.substring(0 , 1);
            String digit2 = strNum.substring(1 , 2);
            String digit3 = strNum.substring(2 , 3);

            if ((Integer.parseInt(digit1) + Integer.parseInt(digit2) + Integer.parseInt(digit3)) == 24){
                System.out.println(i);
            }
        }
    }
}
