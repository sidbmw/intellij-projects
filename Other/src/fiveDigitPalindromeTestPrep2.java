public class fiveDigitPalindromeTestPrep2 {
    public static void main(String[] args) {

        int start = 99999;

        for (int j = 0; j > 0; j++) {
            for (int i = start; i > 9999; i--) {
                String strNum = Integer.toString(i);

                if (i % 6 == 0) {
                    String digit1 = strNum.substring(0, 1);
                    String digit2 = strNum.substring(1, 2);
                    String digit4 = strNum.substring(3, 4);
                    String digit5 = strNum.substring(4, 5);

                    if ((Integer.parseInt(digit1) == Integer.parseInt(digit5)) && ((Integer.parseInt(digit2) == Integer.parseInt(digit4)))) {
                        System.out.println(i);
                    }
                }
            }
        }
    }
}
