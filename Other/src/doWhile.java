public class doWhile {
    public static void main(String[] args) {
        int i = 1; // i is going to be our "counter"
        do // start the block of code that will repeat
        {
            System.out.println("Repetition is very important.");
            i++; // increment the loop counter
        } // end loop
        while (i < 10); // continue while i is less than 5
        System.out.println("The value of i is: " + i);
// What is the value of i here?
    }
}
