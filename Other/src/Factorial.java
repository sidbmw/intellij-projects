/*
 * Author: Mr. Roller
 * Date: Feb 14th, 2016
 *
 * Description:  This program calculates the factorial of a user input number
 */

import java.util.Scanner;

public class Factorial {

    public static void main(String[] args) {
        //Declare variables and initialize
        int factorial = 1;

        Scanner input = new Scanner(System.in);

        //Ask for user input
        System.out.println("Enter a positive integer: ");

        if ((input.hasNextInt()) && (input.nextInt() <= 16))    //If we have a legitimate integer
        {
            int number = input.nextInt();   //store the integer in "input"
            if (number > 0)               //if the integer is positive
            {
                for (int i = 1; i <= number; i++)  //calculate factorial
                {
                    factorial = factorial * i;
                }
                System.out.println("The factorial of " + number + " is " + factorial);
            } else {
                System.out.println("Invalid Input");
            }
        } else {
            System.out.println("Invalid Input");
        }
    }
}