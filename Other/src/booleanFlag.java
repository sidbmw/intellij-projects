/*
 * Author: Mrs. Cullum/Mr. Roller
 * Date: Feb 16th, 2016
 * Description: This program demonstrates the use of
 * boolean flags
 */

import java.util.Scanner;

public class booleanFlag {

    public static void main(String[] args) {
        //variables
        int userNumber;
        boolean inList = false;

        //create scanner
        Scanner input = new Scanner(System.in);

        //ask user for a number
        System.out.println("Enter a number: ");
        userNumber = input.nextInt();

        //test to see if the userNumber is in a string of numbers

        for (int i = 0; i <= 2048; i += 7)  //start at 0 and jump by 7's
        {
            System.out.println(i);
            if (userNumber == i) {
                inList = true;
            }
        }

        if (inList)   //Remember an if statement argument always boils down
        //to a boolean
        {
            System.out.println("The input value IS in the list.");
        } else {
            System.out.println("The input value IS NOT in the list.");
        }
        input.close();  //Proper way to close the scanner!

    }

}