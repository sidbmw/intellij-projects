public class fiveDigitPalindromeTestPrep {
    public static void main(String[] args) {
        int fiveDigitNum = 99999;

        int counter = 0;

        while ((fiveDigitNum > 9999) && (counter < 5)) {
            String strNum = Integer.toString(fiveDigitNum);

            if (fiveDigitNum % 6 == 0) {
                String digit1 = strNum.substring(0, 1);
                String digit2 = strNum.substring(1, 2);
                String digit4 = strNum.substring(3, 4);
                String digit5 = strNum.substring(4, 5);

                if ((Integer.parseInt(digit1) == Integer.parseInt(digit5)) && ((Integer.parseInt(digit2) == Integer.parseInt(digit4)))) {
                    System.out.println(fiveDigitNum);
                    counter++;
                }
            }
            fiveDigitNum--;
        }

    }
}