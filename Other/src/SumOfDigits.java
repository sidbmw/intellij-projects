public class SumOfDigits {
    public static void main(String[] args) {

        System.out.print("These are the number combinations that add up to 24: ");

        int testNum = 100;

        while (testNum < 1000 && testNum > 99) {
            String strNum = Integer.toString(testNum);
            String letter = strNum.substring(0, 1);
            String num1 = letter;

            letter = strNum.substring(1, 2);
            String num2 = letter;

            letter = strNum.substring(2, 3);
            String num3 = letter;

            int sumNum = (Integer.parseInt(num1) + Integer.parseInt(num2) + Integer.parseInt(num3));

            if (sumNum == 24) {
                System.out.print(testNum + " ");
            }
            testNum++;
        }
    }
}