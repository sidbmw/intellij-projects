import java.util.Scanner;

public class loopsTest1 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter a int: ");
        int userInput = sc.nextInt();
        String space = " ";
        int n = userInput;

        for (int i = 1; i <= userInput; i++) {

            if (i != 3) {
                System.out.print("*");
                System.out.print("\n" + space);
                space = space + space;
            }

            if (i == 3) {
                System.out.print("%\n");
                System.out.print("   ");
            }

        }
    }
}
