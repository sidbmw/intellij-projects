import java.util.Scanner;

public class loopTest2 {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        System.out.println("enter int: ");
        int n = sc.nextInt();

        for (int i = n; i > 0; i--) {

            n = (i * 4) - 1;
            System.out.print(n + " ");
            n++;
        }
    }
}

//
//import java.util.Scanner;
//
//public class loopTest2 {
//    public static void main(String[] args) {
//
//        Scanner sc = new Scanner(System.in);
//        System.out.println("enter int: ");
//        int n = sc.nextInt();
//        int counter = n;
//
//        for (int i = 1; i <= counter; i++) {
//
//            n = (i * 4) - 1;
//            System.out.print(n + " ");
//            n++;
//        }
//    }
//}