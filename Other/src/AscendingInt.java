public class AscendingInt {
    public static void main(String[] args) {

        int mainNum = 200;

        while (mainNum <= 300) {

            String strNum = Integer.toString(mainNum);
            String digit1 = strNum.substring(0, 1);
            String digit2 = strNum.substring(1, 2);
            String digit3 = strNum.substring(2, 3);

            if ((Integer.parseInt(digit3) > Integer.parseInt(digit2)) && (Integer.parseInt(digit2) > Integer.parseInt(digit1))) {
                System.out.println(mainNum);
            }
            mainNum++;
        }
    }
}
