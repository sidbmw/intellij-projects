import java.util.InputMismatchException;
import java.util.Scanner;

public class IntReverser {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter a positive 4 digit integer: ");
        int userInput = 0;
        //System.out.println("1");

        if (sc.hasNextInt()) {
            userInput = sc.nextInt();
        } else {
            System.out.println("Invalid input");
            System.exit(0);
        }

        while (userInput < 1000 || (userInput > 9999)) {
            System.out.println("inside while");
            try {
                System.out.println("enter int");
                userInput = sc.nextInt();
            } catch (InputMismatchException e) {
                System.out.println("Invalid input!!!");
            }
        }
        System.out.println("2");
        System.out.print("Enter another positive 4 digit integer: ");
        int userInput2 = sc.nextInt();

        System.out.println(userInput + " " + userInput2);
    }
}