import java.util.Scanner;
public class WhileLoops {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int total = 0;
        int number = 0;

        while (number >= 0 && total <= 1000){
            total += input.nextInt();
            System.out.println(total);
        }
    }
}
