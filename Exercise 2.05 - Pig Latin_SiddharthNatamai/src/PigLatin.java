import java.util.Scanner;
public class PigLatin {

//    Scanner sc = new Scanner();
//    String newSentence = sc.nextLine();
    public static String translateWord(String word)
    {
        if(isVowel(word.charAt(0)))
            return word + "-ay";

        if(word.charAt(0) == 'q' && word.charAt(1) == 'u')
            return word.substring(2) + "-" + word.substring(0,2) + "ay";

        int vowelIndex = vowelFinder(word);
        return word.substring(vowelIndex) + "-"
                + word.substring(0, vowelIndex) + "ay";
    }

    public static String translateSentence(String sentence)
    {

        String newSentence = "";

        while(sentence.length() > 0)
        {
            int isCommaPresent = -1;
            int indexOfSpace = sentence.indexOf(" ");
            String word = "";

            if (indexOfSpace < 0)
                word = sentence;
            else
                word = sentence.substring(0, indexOfSpace);

            isCommaPresent = word.indexOf(",");

            if (isCommaPresent >= 0)
                word = word.replace(",","");

            word = translateWord(word);

            if (isCommaPresent >= 0)
                word = word + ",";

            newSentence = newSentence + " " + word;

            if (indexOfSpace < 0)
                sentence = "";
            else
                sentence = sentence.substring(indexOfSpace+1);
        }

        return newSentence;
    }

    private static boolean isVowel(char letter)
    {
        if (letter == 'a' || letter == 'e' ||
                letter == 'i' || letter == 'o' ||
                letter == 'u' || letter == 'y')
            return true;

        return false;
    }

    private static int vowelFinder(String w)
    {
        for (int i = 0; i < w.length(); i++)
        {
            if(isVowel(w.charAt(i)))
                return i;
        }
        return 0;
    }

}