import javax.swing.*;
import java.awt.event.*;
import java.awt.*;

/**
 * This frame will provide a graphical user interface (GUI) that allows
 * someone to translate easily from English text into Pig Latin
 * (NOTE: This will be our first official example of inheritance!)
 *
 * @author Ms Cianci
 * @modifications Mr. Roller
 * @since April 11th, 2016
 */
public class PigLatinGUI extends JFrame implements ActionListener {
    private JButton translateButton, instructionsButton;
    private JTextArea englishArea, pigLatinArea;

    /**
     * Constructor method sets up the frame for the Pig Latin translator's
     * graphical user interface
     */
    public PigLatinGUI() {
        setSize(600, 400);
        setResizable(false);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setLayout(null);
        setTitle("Pig Latin GUI");

        Container c = getContentPane();
        c.setBackground(new Color(0, 150, 0));

        JLabel titleLabel = new JLabel("Pig Latin Translator");
        titleLabel.setBounds(50, 10, getWidth(), 40);
        titleLabel.setFont(new Font("Georgia", Font.BOLD, 26));
        titleLabel.setForeground(Color.YELLOW);
        c.add(titleLabel);

        JLabel englishLabel = new JLabel("English");
        englishLabel.setBounds(445, 44, getWidth(), 20);
        englishLabel.setFont(new Font("Georgia", Font.BOLD, 12));
        englishLabel.setForeground(Color.BLACK);
        c.add(englishLabel);

        JLabel pigLatinLabel = new JLabel("Translated Pig Latin");
        pigLatinLabel.setBounds(400, 184, getWidth(), 20);
        pigLatinLabel.setFont(new Font("Georgia", Font.BOLD, 12));
        pigLatinLabel.setForeground(Color.BLACK);
        c.add(pigLatinLabel);

        englishArea = new JTextArea();
        englishArea.setBounds(360, 70, 215, 100);
        englishArea.setLineWrap(true);
        englishArea.setWrapStyleWord(true);
        englishArea.setFont(new Font("Georgia", Font.PLAIN, 15));
        c.add(englishArea);

        pigLatinArea = new JTextArea();
        pigLatinArea.setBounds(360, 210, 215, 100);
        pigLatinArea.setLineWrap(true);
        pigLatinArea.setWrapStyleWord(true);
        pigLatinArea.setFont(new Font("Georgia", Font.PLAIN, 15));
        pigLatinArea.setEditable(false);
        c.add(pigLatinArea);

        //translateButton = new JButton(new ImageIcon("Images/PinkPig.gif"));
        translateButton = new JButton(new ImageIcon("/home/siddharth/Pictures/pigTranslate.png"));
        translateButton.addActionListener(this);
        translateButton.setBounds(50, 50, 240,  240);
        translateButton.setBounds(50, 50, 240, 240);
        translateButton.setOpaque(false);
        translateButton.setBackground(new Color(150, 0, 0));
        translateButton.setBorder(null);
        c.add(translateButton);

        instructionsButton = new JButton("Instructions");
        instructionsButton.addActionListener(this);
        instructionsButton.setBounds(170, 300, 150, 40);
        instructionsButton.setOpaque(true);
        instructionsButton.setBackground(new Color(255, 0, 0));
        c.add(instructionsButton);

        setVisible(true);


    } // end constructor method

    /**
     * If the translate button is pressed, translate the entered text from
     * English to Pig Latin and display it.
     * If the instructions buttons is pressed, display the instructions in a pop-up
     */
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == translateButton) {
            pigLatinArea.setText(PigLatin.translateSentence(englishArea.getText()));
        } else // instructionsButton was pressed
        {
            JOptionPane.showMessageDialog(this, "Simply enter an English sentence in "
                    + "the top work window \n"
                    + "and our professional translator will convert to Pig Latin in \n"
                    + "the bottom results window! ");
        }
    } // end actionPerformed method

    public static void main(String[] args) {
        PigLatinGUI gui = new PigLatinGUI();
    }
} // end class