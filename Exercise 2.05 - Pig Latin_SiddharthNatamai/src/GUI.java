import javax.swing.*;
import java.awt.event.*;
import java.awt.*;

/**
 * This frame will provide a graphical user interface (GUI) that allows
 * someone to translate easily from English text into Pig Latin
 * (NOTE: This will be our first official example of inheritance!)
 *
 * @author Ms Cianci
 * @modifications Mr. Roller
 * @since April 11th, 2016
 */
public class GUI extends JFrame implements ActionListener {
    private JButton button;
    private JTextArea area1, area2;

    /**
     * Constructor method sets up the frame for the Pig Latin translator's
     * graphical user interface
     */
    public GUI() {
        setSize(600, 400);
        setResizable(false);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setLayout(null);
        setTitle("Pig Latin GUI");

        Container c = getContentPane();
        c.setBackground(new Color(0, 150, 0));

        JLabel titleLabel = new JLabel("Pig Latin Translator");
        titleLabel.setBounds(10, 10, getWidth(), 40);
        titleLabel.setFont(new Font("Georgia", Font.BOLD, 26));
        titleLabel.setForeground(Color.YELLOW);
        c.add(titleLabel);

        area1 = new JTextArea();
        area1.setBounds(360, 70, 215, 100);
        area1.setLineWrap(true);
        area1.setWrapStyleWord(true);
        area1.setFont(new Font("Georgia", Font.PLAIN, 15));
        c.add(area1);

        area2 = new JTextArea();
        area2.setBounds(360, 210, 215, 100);
        area2.setLineWrap(true);
        area2.setWrapStyleWord(true);
        area2.setFont(new Font("Georgia", Font.PLAIN, 15));
        area2.setEditable(false);
        c.add(area2);

        button = new JButton("Hello ICS3U!");
        button.addActionListener(this);
        button.setBounds(0, 100, 340, 200);
        button.setOpaque(true);
        button.setBackground(new Color(150, 150, 150));
        button.setBorder(null);
        c.add(button);

        setVisible(true);


    } // end constructor method

    /**
     * If the translate button is pressed, translate the entered text from
     * English to Pig Latin and display it.
     * If the instructions buttons is pressed, display the instructions in a pop-up
     */
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == button) {
            area1.setText("You pushed the button!");
            area2.setText("You wrote: " + area1.getText());

        }
    } // end actionPerformed method

    public static void main(String[] args) {
        new GUI();
    }
} // end class