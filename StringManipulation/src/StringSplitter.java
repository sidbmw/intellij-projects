/*
 * Author: Siddharth Natamai
 * Date: Nov 23, 2017
 * Description:  This program accepts a sentence as input and prints each word
 * one below the other. It also removes all punctuation.
 */

import java.util.Scanner;

public class StringSplitter {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in); //Scanner input
        System.out.print("Enter String: ");
        String userInput = sc.nextLine() + " "; //Scanner input is set to userInput
        userInput = userInput.replace(",", " "); //Remove all occurences of , (comma)
        userInput = userInput.replace("\"", " "); //Remove all occurences of " (double quote)
        userInput = userInput.replace("'", " "); //Remove all occurences of ' (apostrophe)
        userInput = userInput.replace(".", " "); //Remove all occurences of . (period/full stop)
        userInput = userInput.replaceAll("   ", " "); //Remove all occurences of 3 blank spaces
        userInput = userInput.replaceAll("  ", " "); //Remove all occurences of 2 blank spaces


        int end = userInput.indexOf(" "); //Position of the first blank in the string is set
        int start = 0; //Initial position for scanning

        //loop until there are no more blank spaces
        do {
            System.out.println(userInput.substring(start, end));
            start = end + 1; //Advance the start position to the position of the blank + 1
            userInput.indexOf(" ", start);
            end = userInput.indexOf(" ", end + 1);
        } while (end > 0);
    }
}
